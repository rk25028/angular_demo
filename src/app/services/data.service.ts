import { Injectable } from '@angular/core'; 
import { Http} from '@angular/http';
import { HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';



@Injectable()
export class DataService{
    url:string = "https://jsonplaceholder.typicode.com/posts";
    constructor( private http: Http ){}

    

    GetCoachClientList() {
        return this.http.get(this.url)
                   .map(res => res.json());
                  // .catch(this.handleErrorObservable);
    }
}