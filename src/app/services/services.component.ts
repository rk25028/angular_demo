import { Component, OnInit ,} from '@angular/core';
import { HttpClient , HttpHeaders}  from '@angular/common/http';
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import { trigger, style, animate, transition, keyframes, query, stagger } from '@angular/animations';
import { DataService } from './data.service';
import { Console } from '@angular/core/src/console';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
  animations :[
      trigger('goals',[
          transition('* => *',[
             query(':enter', style({ opacity: 0 }), { optional : true }),
             query(':enter', stagger('300ms',[
               animate('0.6s ease-in', keyframes([
                 style({ opacity : 0, transform: 'translateY(-75%)', offset : 0 }),
                 style({ opacity : .3, transform: 'translateY(35px)', offset : .3 }),
                 style({ opacity : 1, transform: 'translateY(0)', offset : 1 })
               ]))]), { optional : true })
          ])
      ])
  ]
})

export class ServicesComponent implements OnInit {
 private posts  = []; 
 itemcount : number ;
 goalText : string = "All Goals";
 goals = ['My first goal ', 'My Second goal ','My Third goal '];
 data: any;
 length: any;
   
 constructor( private MyService : DataService) { 
   
  this.MyService.GetCoachClientList().subscribe( posts => this.posts = posts);
  this.length = this.MyService.GetCoachClientList().subscribe(posts => posts.lenght);
  //alert(length);
 // console.log(this.posts);
  
} 
  

  ngOnInit() {
     this.itemcount = this.goals.length;
  }

  addItem(){
    
    this.goals.push(this.goalText);
    this.goalText = ' ';
    this.itemcount = this.goals.length;
  }




}
