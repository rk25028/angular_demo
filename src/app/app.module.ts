import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { HttpClientModule } from '@angular/common/http';
import { HttpModule} from '@angular/http';
import { Routes, Router }  from '@angular/router';
import { routes } from './app.routes';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { ServicesComponent } from './services/services.component';
import { HomeComponent } from './home/home.component';
import { LocationStrategy, HashLocationStrategy }  from '@angular/common';
import { DataService } from './services/data.service';
import { InterceptorModule } from './access-control.module'


@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ServicesComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    routes
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },DataService],
  bootstrap: [AppComponent]
})
export class AppModule {

 
 }

